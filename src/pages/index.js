import React from 'react';
import classnames from 'classnames';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

const features = [
  {
    title: <>Programmatic Constraint-Based Modeling</>,
    imageUrl: 'img/code.svg',
    description: (
      <>
        Develop experiment models that rigorously define the domain of 
        applicability. A code-driven approach to experiment
        developemnt with powerful tools for compilation, static analysis
        and model fragment generation.
      </>
    ),
  },
  {
    title: <>Integrated Emulation and Simulation</>,
    imageUrl: 'img/emulation.svg',
    description: (
      <>
        Describe the environment in which your experiment exists using
        emulated networks and computers, simulated physical systems and
        agents that model human behavior.
      </>
    ),
  },
  {
    title: <>Testbed as a Platform</>,
    imageUrl: 'img/racks.svg',
    description: (
      <>
        Run your experiments on one of our testbed facilities, join our
        testbed ecosystem as a resource provider, or build your own
        distributed system of testbeds using Merge technology.  MergeTB is 
        a modular platform for building testbed ecosystems.
      </>
    ),
  },
];

function Feature({imageUrl, title, description}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={classnames('col col--4', styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3>{title}</h3>
      <p>{description}</p>
    </div>
  );
}

function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title={`${siteConfig.title}`}
      description=" The Merge testbed platform <head />">
      <header className={classnames('hero hero--dark', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">
            Conduct&nbsp;
            <span className={classnames('heroProjectKeywords')}> 
                experimental research 
            </span> 
            &nbsp;on&nbsp;
            <span className={classnames('heroProjectKeywords')}> 
                networked systems.
            </span> 
          </h1>
          {/* <p className="hero__subtitle">{siteConfig.tagline}</p> */}
          <div className={styles.buttons}>
            <Link
              className={classnames(
                'button button--primary button--lg',
                styles.getStarted,
              )}
              to={useBaseUrl('docs/')}>
              Get Started
            </Link>
          </div>
        </div>
      </header>
      <main>
        {features && features.length > 0 && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}

export default Home;

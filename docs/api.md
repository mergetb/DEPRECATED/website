---
id: api
title: Merge API
---

The Merge API is an OpenAPI 2.0 spec API. An API doc is located
[here](/api.html).

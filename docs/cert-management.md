---
id: cert-management
title: Certificate Management
---

Many of the Merge components require TLS certificates for secure and encrypted
communication. We recommend using a tool called [cfssl](https://github.com/cloudflare/cfssl) for
certificate generation. Here is an example of generating a certificate authority
along with a commander certificate

## Certificate Authority Configuration

ca-config.json
```json
{
  "signing": {
    "default": {
      "expiry": "4700h"
    },
    "profiles": {
      "spineleaf": {
        "usages": ["signing", "key encipherment", "server auth", "client auth"],
        "expiry": "4700h"
      }
    }
  }
}
```


##  Certificate Authority Certificate Signing Request

ca-csr.json
```json
{
  "CN": "spineleaf",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Marina del Rey",
      "ST": "California",
      "O": "spineleaf",
      "OU": "CA"
    }
  ]
}
```

## Commander Certificate Signing Request

cmdr-csr.json
```json
{
  "CN": "spineleaf:cmdr",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Marina del Rey",
      "ST": "California",
      "O": "spineleaf",
      "OU": "datastores"
    }
  ]
}
```

## Generate Certificate Authority
```shell
./cfssl gencert -initca ca-csr.json | ./cfssljson -bare ca
```

## Generate Commander Certificate
```shell
./cfssl gencert                                         \
  -ca=ca.pem                                            \
  -ca-key=ca-key.pem                                    \
  -config=ca-config.json                                \
  -hostname=spineleaf.mergetb.test,localhost,127.0.0.1  \
  -profile=spineleaf                                    \
  cmdr-csr.json | ./cfssljson -bare cmdr

```



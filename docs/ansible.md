---
id: automation-basics
title: Experiment Automation Basics
---

Ansible is the recommended tool for experiment automation. This guide will go
over the basics of using Ansible for Merge experiments, the general Ansible docs
can be found [here](https://docs.ansible.com/).

Ansible allows you to automate a collection of experiment nodes from a
centralized file called a **playbook**.

![](/img/xpdev/playbook.png)

We recommend executing playbooks from an XDC attached to your experiment
materialization.

## Topology

The following topology will be used in this example. It's the simple two node
topology depicted in the diagram above.

```python
import mergexp as mx

net = mx.Topology('ansible')

nodes = a, b = [net.device(name) for name in ['a', 'b']]
link = net.connect(nodes)
link[a].ip.addrs = ['10.0.0.1/24']
link[b].ip.addrs = ['10.0.0.2/24']

mx.experiment(net)
```

## Playbooks
A playbook is a sequence of plays that can be anything from provisioning users,
copying files, configuring software to running generic shell commands and
scripts. Here is an example of a playbook that installs and runs the `iperf3`
network performance tool across two nodes. Here we assume the nodes are called
named `a` and `b` and that the experiment network IP addresses of these nodes
are 10.0.0.1 and 10.0.0.2 respectively.

### `playbook.yml`
```yaml
- hosts: all
  become: true
  tasks:
    - name: install iperf3
      apt:
        name: iperf3

- hosts: b
  tasks:
    - name: run iperf3 in server mode
      shell: iperf3 --server --daemon --one-off

- hosts: a
  tasks:
    - name: run iperf3 in client mode
      shell: iperf3 -c 10.0.0.2 --logfile /tmp/results

    - name: collect results
      fetch: 
        src: /tmp/results
        dest: results
        flat: yes
```

## Inventories

Ansible requires a file called an inventory to tell it how to reach the nodes
it's automating. This file is typically called `hosts`. Here is an example for
our two node topology.

### `hosts`
```toml
[all]
a
b
```

Names in square brackets indicate groups, the names below the square brackets
are the members of a group. The `all` group was used in the first sequence of
plays above, and then individual nodes were used for the follow-on play
sequences.

## Configuration

Ansible configuration is kept in a file called `ansible.cfg`. This file can be
global at `/etc/ansible/ansible.cfg`, or it can be placed in the local directory
where you execute a playbook from. There are a few options that are very useful
to set for executing from an XDC.

```toml
[defaults]

# don't check experiment node keys, if this is not set, you will have to
# explicitly accept the SSH key for each experiment node you run Ansible 
# against
host_key_checking = False

# configure up to 5 hosts in parallel
forks = 5

# connection optimization that increases speed significantly
pipelining = True
```

## Execution

To execute the `playbook.yml` above we simply type

```shell
ansible-playbook -i hosts playbook.yml
```

After executing the playbook, you should have a results file in your local
directory like the following

```text
$ cat results
Connecting to host 10.0.0.2, port 5201
[  6] local 10.0.0.1 port 40620 connected to 10.0.0.2 port 5201
[ ID] Interval           Transfer     Bitrate         Retr  Cwnd
[  6]   0.00-1.00   sec   114 MBytes   954 Mbits/sec    0    437 KBytes
[  6]   1.00-2.00   sec   113 MBytes   947 Mbits/sec    0    437 KBytes
[  6]   2.00-3.00   sec   112 MBytes   941 Mbits/sec    0    437 KBytes
[  6]   3.00-4.00   sec   112 MBytes   942 Mbits/sec    0    482 KBytes
[  6]   4.00-5.00   sec   112 MBytes   941 Mbits/sec    0    482 KBytes
[  6]   5.00-6.00   sec   111 MBytes   929 Mbits/sec   75    400 KBytes
[  6]   6.00-7.00   sec   112 MBytes   943 Mbits/sec    0    426 KBytes
[  6]   7.00-8.00   sec   113 MBytes   945 Mbits/sec    0    426 KBytes
[  6]   8.00-9.00   sec   112 MBytes   941 Mbits/sec    0    426 KBytes
[  6]   9.00-10.00  sec   113 MBytes   946 Mbits/sec    0    426 KBytes
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bitrate         Retr
[  6]   0.00-10.00  sec  1.10 GBytes   943 Mbits/sec   75             sender
[  6]   0.00-10.04  sec  1.09 GBytes   937 Mbits/sec                  receiver
```

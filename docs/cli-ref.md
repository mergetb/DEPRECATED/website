---
id: cli-ref
title: Experimenter CLI
---

## Basics

Merge has a powerful command line client. It's designed to be used for both
user interaction and script integration. **_All commands that return data support
the `--json` flag which produces json output consumable by a script._** Regular
text command output should not be used for scripting. We make absolutely zero
guarantees for the stability of plaintext output. Plaintext output also contains 
shell control characters for color coding.

Many commands deal with elements that are part of a project, such as
experiments, realizations and materializations. In the absence of the
`--project` flag, the project in use is assumed to be the calling users personal
project. If a different project is desired, the `--project` flag may be used to
specify it.

All commands support **_prefix shortcuts_**. For example the command

```shell
mergetb show experiment phobos
```

can be expressed as 

```shell
mergetb sh exp phobos
```

Only commands can be abbreviated, arguments to commands cannot.


## Authentication

The command line client uses the Merge OAuth2 system for authentication. Before
using the client, you must login using the [login](#login) command.

The API used by the `mergetb` tool exposes a TLS endpoint only. For portals with 
self signed certs, or development environments, the `--cert` flag may be used to 
augment your systems CA pool with an additional CA chain from a PEM file.

## Command structure

### Administrative

- [join](#join)
- [login](#login)
- [logout](#logout)
- [version](#version)

### Project management

- [project add](#add-project-member)
- [project delete](#delete-project-member)
- [project list](#list-project-members)

### Experiment management

- [new experiment](#new-experiment)
- [delete experiment](#delete-experiment)
- [show experiment](#show-experiment)
- [list experiments](#list-experiments)
- [pull](#pull)
- [push](#push)
- [history](#history)

### Resource management

- [list resources](#list-resources)

### Realization management

- [realize](#realize)
- [free](#free)
- [accept](#accept)
- [reject](#reject)
- [show realization](#show-realization)
- [list realizations](#list-realizations)

### Materialization management

- [materialize](#materialize)
- [dematerialize](#dematerialize)
- [status](#status)
- [show materialization](#show-materialization)


### Materialization connection

- [addkey](#addkey)
- [rmkey](#rmkey)

## Command reference

### Administrative

#### Join

The join command adds your account to a Merge portal. This is the first step to
getting going on any Merge portal. Your account will not be able to access the 
API until an administrator activates it.

```shell
$ mergetb join -h
Join a Merge portal

Usage:
  mergetb join [flags]

Flags:
  -h, --help   help for join

Global Flags:
  -c, --cert string      TLS cert to use for connecting to portal
  -p, --project string   project to use (defaults to personal project)
```

#### Login

The login command attempts to log a user into a Merge portal instance using an
OAuth2 authentication flow. Upon successful login two files are created

- `~/.merge/id`: the username used to login
- `~/.merge/token`: the token acquired from the authentication endpoint

The token is only good for a fixed period of time. How long depends on how the 
policy of the particular portal. A typical token lifetime is around 24 hours.

```shell
$ mergetb login -h
Login to the Merge portal

Usage:
  mergetb login <username> [flags]

Flags:
  -h, --help   help for login

Global Flags:
  -c, --cert string      TLS cert to use for connecting to portal
  -p, --project string   project to use (defaults to personal project)
```

#### Version

Print the version of the CLI tool

```shell
$ mergetb version -h
Print CLI version

Usage:
  mergetb version [flags]

Flags:
  -h, --help   help for version

Global Flags:
  -c, --cert string      TLS cert to use for connecting to portal
  -p, --project string   project to use (defaults to personal project)
```

### Project management

#### Add project member

Add a new member to a project.

```shell
$ mergetb project add -h
add a member <project> <member>

Usage:
  mergetb project add [flags]

Flags:
  -h, --help           help for add
  -r, --role string    creator, maintainer, or member (default "member")
  -s, --state string   pending or active (default "active")

Global Flags:
  -c, --cert string      TLS cert to use for connecting to portal
  -p, --project string   project to use (defaults to personal project)
```


#### Delete project member

Delete a member from a project

```shell
$ mergetb project delete -h
delete a member

Usage:
  mergetb project delete <project> <member> [flags]

Flags:
  -h, --help   help for delete

Global Flags:
  -c, --cert string      TLS cert to use for connecting to portal
  -p, --project string   project to use (defaults to personal project)
```

#### List project members

List the members of a project.

```shell
$ mergetb project list -h
list members

Usage:
  mergetb project list <project> [flags]

Flags:
  -h, --help   help for list

Global Flags:
  -c, --cert string      TLS cert to use for connecting to portal
  -p, --project string   project to use (defaults to personal project)
```

### Experiment management

#### New experiment

Create a new experiment. This will create a new experiment under your personal
project, unless the `--project` flag is used to specify a different project.

The `--desc` flag may be used to provide an optional text description.

The `--src` flag may point to a topology written in Python using the [Merge
Experimentation Library](https://pypi.org/project/mergexp/). When specified, the Merge CLI utility will attempt
to run the topology script, extract the corresponding XIR and use it as the
initial source version for the new experiment.

```shell
$ mergetb new experiment -h
Create a new experiment

Usage:
  mergetb new experiment <name> [flags]

Flags:
  -d, --desc string   description
  -h, --help          help for experiment
  -s, --src string    source mx file

Global Flags:
  -c, --cert string      TLS cert to use for connecting to portal
  -p, --project string   project to use (defaults to personal project)
```

#### Delete experiment

Delete an experiment.

```shell
$ mergetb delete experiment -h
Delete an experiment

Usage:
  mergetb delete experiment <name> [flags]

Flags:
  -h, --help   help for experiment

Global Flags:
  -c, --cert string      TLS cert to use for connecting to portal
  -p, --project string   project to use (defaults to personal project)
```

#### Show experiment

Show experiment details

```shell
$ mergetb show experiment -h
Get experiment show

Usage:
  mergetb show experiment <name> [flags]

Flags:
  -h, --help   help for experiment

Global Flags:
  -c, --cert string      TLS cert to use for connecting to portal
  -j, --json             output as json
  -p, --project string   project to use (defaults to personal project)
```

#### List experiments

List experiments belonging to a particular project. Defaults to personal
project, use `--project` to switch projects.

```shell
$ mergetb list experiments -h
List experiments

Usage:
  mergetb list experiments [flags]

Flags:
  -h, --help   help for experiments

Global Flags:
  -c, --cert string      TLS cert to use for connecting to portal
  -j, --json             output as json
  -p, --project string   project to use (defaults to personal project)
```

#### Pull

Pull experiment source XIR. The `name` argument is the name of the experiment
and `hash` is the version hash you wish to pull.

```shell
$ mergetb pull -h
Pull experiment version

Usage:
  mergetb pull <name> <hash> [flags]

Flags:
  -h, --help   help for pull

Global Flags:
  -c, --cert string      TLS cert to use for connecting to portal
  -p, --project string   project to use (defaults to personal project)
```

#### Push

Push a new version of an experiment. The `source.py` argument is a Python3
topology script, written using the [Merge Experimentation Library](https://pypi.org/project/mergexp/).
`mergetb` utility will attempt to run the Python script and extract XIR from the
topology. The extracted XIR is then pushed to the portal as the pushed version.

```shell
$ mergetb push -h
Push experiment version

Usage:
  mergetb push <name> <source.py> [flags]

Flags:
  -h, --help   help for push

Global Flags:
  -c, --cert string      TLS cert to use for connecting to portal
  -p, --project string   project to use (defaults to personal project)
```

#### History

Show the version history of an experiment. This command will list all the
version hashes of an experiment in chronological order.

```shell
$ mergetb history -h
Get experiment history

Usage:
  mergetb history <name> [flags]

Flags:
  -h, --help   help for history

Global Flags:
  -c, --cert string      TLS cert to use for connecting to portal
  -p, --project string   project to use (defaults to personal project)

```

### Resource management

#### List resources

List available resources.

```shell
 mergetb list resources -h
List available resources

Usage:
  mergetb list resources <name> [flags]

Flags:
  -h, --help   help for resources

Global Flags:
  -c, --cert string      TLS cert to use for connecting to portal
  -j, --json             output as json
  -p, --project string   project to use (defaults to personal project)
```

### Realization management

#### Realize

Realize an experiment. Realization is the process of choosing and allocating
resources for an experiment.

When a realization is submitted, if sufficient
resources are available for the entire experiment, a pending allocation for all
the selected resources will be created. This pending allocation lasts 47
seconds. During this time window the realization may be explicitly
[accepted](#accept) or [rejected](#reject) by the user. No action within 47
seconds is an implicit reject. On rejection, all allocations are released.  The `--accept` flag can be used to automatically accept a successful realization.

By default, the most recent experiment hash is used for realization. If an older
version is desired, the `--version` flag may be used to specify a specific
version hash.

```shell
$ mergetb realize  -h
Realize an experiment

Usage:
  mergetb realize <experiment> <name> [flags]

Flags:
  -a, --accept           accept any successful realization
  -h, --help             help for realize
  -v, --version string   experiment source version

Global Flags:
  -c, --cert string      TLS cert to use for connecting to portal
  -p, --project string   project to use (defaults to personal project)
```

#### Free

Free releases resources allocated to a realization and deletes the
realization.

```shell
$ mergetb free -h
Unrealize an experiment

Usage:
  mergetb free <experiment> <name> [flags]

Flags:
  -h, --help   help for free

Global Flags:
  -c, --cert string      TLS cert to use for connecting to portal
  -p, --project string   project to use (defaults to personal project)
```

#### Accept

Accept transitions a realization in the pending state to the accepted state.
This means that the allocated resources will belong to the calling user until
explicitly freed.

```shell
$ mergetb accept -h
Accept a realization

Usage:
  mergetb accept <experiment> <name> [flags]

Flags:
  -h, --help   help for accept

Global Flags:
  -c, --cert string      TLS cert to use for connecting to portal
  -p, --project string   project to use (defaults to personal project)
```

#### Reject

Reject deletes a pending realization and removes all associated allocations.

```shell
$ mergetb reject -h
Reject a realization

Usage:
  mergetb reject <experiment> <name> [flags]

Flags:
  -h, --help   help for reject

Global Flags:
  -c, --cert string      TLS cert to use for connecting to portal
  -p, --project string   project to use (defaults to personal project)
```

#### Show realization

Shows the details of a realization.

```shell
$ mergetb show realization -h
Show realization show

Usage:
  mergetb show realization <experiment> <name> [flags]

Flags:
  -h, --help   help for realization

Global Flags:
  -c, --cert string      TLS cert to use for connecting to portal
  -j, --json             output as json
  -p, --project string   project to use (defaults to personal project)
```

### List realizations

List the realizations associated with a particular experiment.

```shell
$ mergetb list realizations -h
List experiment realizations

Usage:
  mergetb list realizations <experiment> [flags]

Flags:
  -h, --help   help for realizations

Global Flags:
  -c, --cert string      TLS cert to use for connecting to portal
  -j, --json             output as json
  -p, --project string   project to use (defaults to personal project)
```

### Materialization management

#### Materialize

Materialize a realization.

```shell
$ mergetb materialize -h
Materialize an experiment

Usage:
  mergetb materialize <experiment> <realization> [flags]

Flags:
  -h, --help   help for materialize

Global Flags:
  -c, --cert string      TLS cert to use for connecting to portal
  -p, --project string   project to use (defaults to personal project)
```

#### Dematerialize

Dematerialize a materialization.

```shell
$ mergetb dematerialize -h
Dematerialize an experiment

Usage:
  mergetb dematerialize <experiment> <realization> [flags]

Flags:
  -h, --help   help for dematerialize

Global Flags:
  -c, --cert string      TLS cert to use for connecting to portal
  -p, --project string   project to use (defaults to personal project)
```

#### Status

Check on the status of a materialization.

```shell
$ mergetb status -h
Get materialization status

Usage:
  mergetb status <experiment> <realization> [flags]

Flags:
  -h, --help   help for status

Global Flags:
  -c, --cert string      TLS cert to use for connecting to portal
  -p, --project string   project to use (defaults to personal project)
```

#### Show materialization

Show the details of a materialization.

```shell
$ mergetb show materialization -h
Get materialization show

Usage:
  mergetb show materialization <experiment> <realization> [flags]

Flags:
  -h, --help   help for materialization

Global Flags:
  -c, --cert string      TLS cert to use for connecting to portal
  -j, --json             output as json
  -p, --project string   project to use (defaults to personal project)
```
#### Addkey

Add personal keying material to the wireguard enclave for a given materialization. 

```shell
The given public key will be added to the wireguard interface on the gateway of the materialization. The gateway's information will be returned so the caller can join the enclave "by hand" via standard wg commands.

Usage:
  mergetb wg addkey <experiment> <realization> <pubkey> [flags]

Flags:
  -h, --help   help for addkey

Global Flags:
  -c, --cert string      TLS cert to use for connecting to portal
  -p, --project string   project to use (defaults to personal project)
```

#### Rmkey

Remove personal keying material from a wireguard enclave for a given materialization. 

```shell
rmKey the wireguard enclave of the given materialization

Usage:
  mergetb wg rmkey <experiment> <realization> <pubkey> [flags]

Flags:
  -h, --help   help for rmKey

Global Flags:
  -c, --cert string      TLS cert to use for connecting to portal
  -p, --project string   project to use (defaults to personal project)
```


---
id: package-server
title: The Merge Package Server
---

Merge has a dedicated Debian package server. The Merge packages are currently
distributed under the following Debian releases

- buster
- kass

Kass is a Merge specific release based on Bullseye for packaging the Moa network
emulation components that have a custom Linux kernel, libc and other low level
packages that are incompatible with standard Debian installations.

## Installation

The easiest way to install the Merge package repo is as follows

```shell
curl pkg.mergetb.net/addrepo | RELEASE=<release> bash -
```

Replace `<release>` with the distribution release you wish to use. This will add
the following to `/etc/apt/sources.list.d/mergetb-<release>`

```shell
deb [arch=amd64] https://pkg.mergetb.net/debian <release> main
```


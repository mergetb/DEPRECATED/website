---
id: vte
title: Virtual Testbed Environment
---

The virtual testbed environment (VTE) is a self contained Merge system that's
deployable as a set of interconnected virtual machines with a single command.

- https://gitlab.com/mergetb/prototypes/tree/master/integrated

The primary components of the VTE are:

- A fully functional Kubernetes based Merge portal deployment.
- A small test site consisting of
    - 8 testbed nodes
    - A fully functional switching and routing mesh based on CumulusVX
    - A site automation node that runs the Cogs system
    - A set of storage servers that serve OS images to nodes and provide network
      mass storage to experiments.
    - A pair of network emulation nodes

When the VTE starts up, all of the MergeTB components are installed on the VTE
virtual machines based on the latest packages that have been produced from the 
continuous integration (CI) process of each project. The VTE serves two primary 
purposes:

- A testing and evaluation environment for testbed developers.
- A fully integrated environment for end to end CI testing.

## System Requirements

At the time of writing the VTE is made up of 28 virtual machines and requires a
fairly significant amount of resources to run.

### Minimum

- A processor with hardware virtualization support.
- 6 Cores
- 64GB RAM
- 450G SSD disk (HDDs are too slow, don't try)

### Recommended

- AMD Threadripper or Epyc processor.
- 16+ Cores
- 128+ GB RAM >= 3000MHz, quad channel+
- 1TB+ NVMe dedicated (read: not running OS) disk

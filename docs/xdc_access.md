---
id: xdc-access
title: XDC Access
sidebar_label: XDC Access
---

Various forms of Ubuntu have a buggy SSH. If you use Ubuntu first re-evaluate
your life decisions and what has led you to this point, then do the following.

Add this to your `~/.ssh/config` replacing anything within `<>` brackets with an
appropriate value.

- `<user>` is your MergeTB user name.
- `<id_rsa>` is the private key corresponding to the public key you uploaded to
  the Merge Portal.
- `<project>` is the name of project containing XDCs you want to connect to.

```config
Host *-<project>
  Hostname %h
  User <user>
  ProxyCommand ssh -p 2202 -i ~/.ssh/<id_rsa> -W %h:%p <user>@jumpc.mergetb.io
  IdentityFile ~/.ssh/<id_rsa>
```

Then you can SSH into an XDC with

```shell
ssh <xdc-name>-<experiment-name>-<project>
```

such as 

```shell
ssh xdc47-delta-quadrant
```

For non-buggy SSH, you can simply use

```shell
Host *-<project>
  Hostname %h
  User <user>
  ProxyJump <user>@jumpc.mergetb.io:2202
```

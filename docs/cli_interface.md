---
id: cli
title: Command Line Interface
sidebar_label: Command Line Interface
---

This is a minimal guide to get a functioning experiment. Start by creating a
file called `hello-world.py`. The library in use here is the Merge Experiment
Library. You can find it on [pypi](https://pypi.org/project/mergexp/), through
pip
```shell
pip3 install mergexp
```
or from install from [source](https://gitlab.com/mergetb/xir/tree/master/lang/mx).

```python
import mergexp as mx

net = mx.Topology('hello-world')

hello = net.device('hello')
world = net.device('world')

link = net.connect([hello, world])

link[hello].ip.addrs = ['10.0.0.1/24']
link[world].ip.addrs = ['10.0.0.2/24']

mx.experiment(net)
```

Now use the `mergetb` command line tool to create, realize and materialize the
experiment. The `mergetb` command line tool is available for download
[here](https://gitlab.com/mergetb/cli/-/jobs/artifacts/master/browse/build?job=build).

```shell
# create an experiment from the topology source
mergetb new exp hello --src hello-world.py

# realize the experiment (allocates resources)
mergetb realize hello initial --accept

# materialize the experiment (actually create the experiment)
mergetb materialize hello initial
```

## Learn more

### Topology modeling

Examples of more interesting topologies and network modeling features are in the
[topology modeling](/docs/routes/) section to the right.

### Command line reference

The [command line reference docs](/docs/cli-ref) provide a comprehensive reference to
the Merge command line utility.

---
id: fattree
title: Fattree
---

Behold, the fattree.

![](/img/topo/fattree.png)

### Complete Example

```python
#!/usr/bin/python3
import mergexp as mx

topo = mx.Topology('fattree')

def spine(index):
    d = topo.device('spine%d'%index)
    d.props['color'] = "#575ced"
    return d

def leaf(index):
    d = topo.device('leaf%d'%index)
    d.props['color'] = "#9497f3"
    return d

def tor(index):
    d = topo.device('tor%d'%index)
    d.props['color'] = "#ea483c"
    return d

def node(index):
    d = topo.device('node%d'%index)
    return d

# topology parameters
nspine, spine_radix  = 4, 4
nleaf,  leaf_radix   = 8, 4
ntor,   tor_radix    = 8, 4
nnode                = 4*ntor

spine = [spine(i) for i in range(nspine)]
leaf =  [leaf(i) for i in range(nleaf)]
tor =   [tor(i) for i in range(ntor)]
node =  [node(i) for i in range(nnode)]

# node to tor
for i,n in enumerate(node):
    topo.connect([n, tor[i//4]])

# tor to leaf
up = tor_radix//2
for i,t in enumerate(tor):
    for j in range(tor_radix//2):
        topo.connect([t,leaf[(i*up+j)%ntor]])

# leaf to spine
up = leaf_radix//2
for i,a in enumerate(leaf):
    for j in range(up):
        topo.connect([a, spine[(i*up+j)%nspine]])

mx.experiment(topo)
```
